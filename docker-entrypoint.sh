#!/usr/bin/env bash
set -eo pipefail

# default variables
: "${SLEEP:=1}"
: "${TRIES:=60}"

function wait_for_database {(
  echo "Waiting for database to respond..."
  tries=0
  while true; do
    [[ $tries -lt $TRIES ]] || return
    (echo "from django.db import connection; connection.connect()" | umap shell) >/dev/null 2>&1
    [[ $? -eq 0 ]] && return
    sleep $SLEEP
    tries=$((tries + 1))
  done
)}

# Stolen from https://hub.docker.com/u/binhex/
# Set umap user UID.
if [ ! -z "${UMAP_UID}" ]; then
    echo "[info] UMAP_UID defined as '${UMAP_UID}'"
    # set user umap to specified user id (non unique)
    usermod -o -u "${UMAP_UID}" umap &>/dev/null
fi

if [ ! -z "${UMAP_GID}" ]; then
    echo "[info] UMAP_GID defined as '${UMAP_GID}'"
    # set group umap to specified group id (non unique)
    groupmod -o -g "${UMAP_GID}" umap &>/dev/null
fi

echo "[info] Setting permission on uploads dir."
chown -R umap: /srv/umap/uploads

# first wait for the database
wait_for_database
# then migrate the database
umap migrate
# then collect static files
umap collectstatic --noinput
# create languagae files
#umap storagei18n
# compress static files
umap compress
# run uWSGI
exec uwsgi --ini uwsgi.ini
